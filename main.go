package main

import (
	"context"
	"fmt"
	"log"

	"github.com/google/go-github/v48/github"
)

type GithubClient interface {
	ListByOrg(context.Context, string, *github.RepositoryListByOrgOptions) ([]*github.Repository, *github.Response, error)
}

type DefaultClient struct {
	*github.Client
}

func (c DefaultClient) ListByOrg(ctx context.Context, org string, opts *github.RepositoryListByOrgOptions) ([]*github.Repository, *github.Response, error) {
	return c.Repositories.ListByOrg(ctx, org, nil)
}

func GetOrgList(g GithubClient) {
	ctx := context.Background()
	repos, _, err := g.ListByOrg(ctx, "opendatasoft", nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(repos)
}

func main() {

	c := DefaultClient{github.NewClient(nil)}
	GetOrgList(c)

}
