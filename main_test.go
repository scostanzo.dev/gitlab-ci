package main

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/go-github/v48/github"
)

func Test_GetOrgList(t *testing.T) {

	client := SpyGithubClient{}
	GetOrgList(client)
}

type SpyGithubClient struct{}

func (s SpyGithubClient) ListByOrg(ctx context.Context, repo string, opts *github.RepositoryListByOrgOptions) ([]*github.Repository, *github.Response, error) {

	fmt.Println("you are here")

	repos := []*github.Repository{}
	resp := github.Response{}

	return repos, &resp, nil

}
