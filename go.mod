module gitlab.com/scostanzo.dev/gitlab-ci

go 1.18

require github.com/google/go-github/v48 v48.2.1-0.20221216151307-2b4d59656c69

require (
	github.com/google/go-querystring v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
)
